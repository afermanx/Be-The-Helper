const connection = require('../database/connection')
module.exports={
    async create(request, response){
        const {email} = request.body;
        const {password} = request.body;

        const ong = await connection('ongs')
        .where('email', email).andWhere('password',password)
        .select('name')
        .first();


        if(!ong){
            return response.status(400).json({
                error:'no ONG found with this ID'
            });
        
        }

        return response.json(ong)
    }
}