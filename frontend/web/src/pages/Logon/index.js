import React, {useState} from 'react';
import {Link, useHistory} from 'react-router-dom'
import './styles.css';
import logoImg from "../../assets/helper.png"
import peopleLogon from "../../assets/people.gif"


import {FiLogIn} from  'react-icons/fi'



import api from '../../services/api'

export default function Logon(){
    const [email, setEmail]=useState('');
    const [password, setPassword]=useState('');
    const history = useHistory();


    async function handleLogin(e){
        e.preventDefault();

        try {

            const response = await api.post('session', { email, password})

          localStorage.setItem('email', email)
          localStorage.setItem('password', password)
          localStorage.setItem('ongName', response.data.name)
        
         
            history.push('/profile')
        } catch (error) {
            alert('falha no login tente novamente');
            
        }

    }

    return(
       <div className="logon-container">
           <section className="form">
           <img src={logoImg} alt="Be The Hero"/>
           <form onSubmit={handleLogin}>
               <h1>Faça seu Logon</h1>
               <input placeholder="seu E-email"
               value={email}
               onChange={e => setEmail(e.target.value)}
               />
               <input type="password" placeholder="Sua Senha"
                value={password}
                onChange={e => setPassword(e.target.value)}
               
               />
               <button className="button" type='submit'>Entrar</button>

               <Link className='back-link' to="/register" >
                   
               <FiLogIn size={16} color="#e33c17" />
                   Não tenho cadastro</Link>
           </form>

           </section>
           <img src={peopleLogon} alt="Heroes"/>
       </div>

    )
}


