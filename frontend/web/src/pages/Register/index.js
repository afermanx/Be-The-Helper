import React,{useState}  from 'react'
import {Link, useHistory} from 'react-router-dom'


import './style.css'
import logoImg from "../../assets/helper.png"
import {FiArrowLeft} from  'react-icons/fi'


import api from '../../services/api'

export default function Register(){
  const [name, setName]=useState('')
  const [email, setEmail]=useState('')
  const [whatsapp, setWhatsapp]=useState('')
  const [city, setCity]=useState('')
  const [uf, setUf]=useState('')
  const [password, setPassword]=useState('')
 
  const history =useHistory();

  
  async function handleRegister(e){
    e.preventDefault();
    const data={
      name,
      email,
      whatsapp,
      city,
      uf,
      password
    }
   try{
    const response = await api.post('ongs', data);
    
    alert(`Seu ID de acesso :${response.data.id}`);
    history.push('/')
   }catch(err){
     alert('erro no cadastro, tente novamente')
   }



  }

    return(
<div className="resgister-container">
<div className="content">
    <section>
    <img src={logoImg}alt="Be The Hero"/>
    <h1>Cadastro</h1>
    <p>Faça seu Cadastro, entre na plataforma se ajude e ajude pessoas com a FACUL.</p>
    <Link className='back-link' to="/" >
                   
               <FiArrowLeft size={16} color="#e02041" />
                  Ja tenho cadastro</Link>
    </section>
    <form onSubmit={handleRegister}>
  <input placeholder="Seu nome Calouro"
  value={name}
  onChange={e => setName(e.target.value)}
  
  />
 <input type="email" placeholder="Seu E-mail Bixo"
value={email}
onChange={e => setEmail(e.target.value)}

/>
<input type="password" placeholder="Sua Senha" 
  value={password}
  onChange={e => setPassword(e.target.value)}
  />
  <input placeholder="Seu Whatsapp, chama la ;)"
  value={whatsapp}
  onChange={e => setWhatsapp(e.target.value)} />


  <div className="input-group">
  <input placeholder="Cidade" 
  value={city}
  onChange={e => setCity(e.target.value)}
  />
  <input placeholder="UF"style={{width:80}}
  value={uf}
  onChange={e => setUf(e.target.value)}
  />

  </div>

 
<button className="button">Cadastrar</button>
    </form>
</div>

</div>
        
        
    )

}